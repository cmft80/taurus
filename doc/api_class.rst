.. _{{modulename}}-{{classname}}:

.. currentmodule:: {{ modulename }}

:class:`{{ classname }}`
{{ '=' * (classname|count + 9 ) }}

.. inheritance-diagram:: {{ classname }}
    :parts: 1

.. autoclass:: {{ classname }}
    :members:
    :undoc-members:

    Import from :mod:`{{modulename}}` as::

        from {{modulename}} import {{ classname }}